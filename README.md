# STM32WB Sandbox

## Notes

### ILI9341 Pins
Pics (per [https://vivonomicon.com/2018/06/17/drawing-to-a-small-tft-display-the-ili9341-and-stm32/](https://vivonomicon.com/2018/06/17/drawing-to-a-small-tft-display-the-ili9341-and-stm32/))
* SCK: Clock
* MOSI: Data output
* MISO: Data input
* CS: Chip select
* DC: Data or command
* RST: Reset


## Links

### Training
* ST's STM32WB Training Page: [https://www.st.com/content/st_com/en/support/learning/stm32-education/stm32-online-training/stm32wb-online-training.html](https://www.st.com/content/st_com/en/support/learning/stm32-education/stm32-online-training/stm32wb-online-training.html)
* Training Videos: 
	* https://www.st.com/content/st_com/en/support/learning/stm32-education/stm32-moocs/STM32WB_workshop_MOOC.html
	* [https://www.youtube.com/watch?v=5Lp9cDBzG94&list=PLnMKNibPkDnGRfqUO1Q_-1nW-tOKfDQbc&index=1](https://www.youtube.com/watch?v=5Lp9cDBzG94&list=PLnMKNibPkDnGRfqUO1Q_-1nW-tOKfDQbc&index=1)


### Useful Software
ST's BSP: [https://github.com/STMicroelectronics/STM32CubeWB](https://github.com/STMicroelectronics/STM32CubeWB)

### Hardware 
Hardware:
* Board Description: [https://www.st.com/resource/en/user_manual/dm00517423-bluetooth-low-energy-and-802154-nucleo-pack-based-on-stm32wb-series-microcontrollers-stmicroelectronics.pdf](https://www.st.com/resource/en/user_manual/dm00517423-bluetooth-low-energy-and-802154-nucleo-pack-based-on-stm32wb-series-microcontrollers-stmicroelectronics.pdf)
* Adafruit 2.8" TFT Touch Shield for Arduino w/Capacitive Touch: [https://www.adafruit.com/product/1947](https://www.adafruit.com/product/1947)
	* https://learn.adafruit.com/adafruit-2-8-tft-touch-shield-v2/connecting
* Modifying the 1947 board to run on 3.3V: 
	* https://community.nxp.com/community/mcuxpresso/blog/2020/01/10/modifying-the-latest-adafruit-28-lcd-for-sdk-graphics-examples
	* https://forums.adafruit.com/viewtopic.php?f=31&t=156474
* BSP
	* Code Repo: [https://github.com/STMicroelectronics/STM32CubeWB](https://github.com/STMicroelectronics/STM32CubeWB)
	* Description: [https://www.st.com/resource/en/application_note/dm00492814-stm32cube-mcu-package-examples-for-stm32wb-series-stmicroelectronics.pdf](https://www.st.com/resource/en/application_note/dm00492814-stm32cube-mcu-package-examples-for-stm32wb-series-stmicroelectronics.pdf)

ILI9341 HALs: 
* [https://github.com/afiskon/stm32-ili9341](https://github.com/afiskon/stm32-ili9341)
* [https://github.com/martnak/STM32-ILI9341](https://github.com/martnak/STM32-ILI9341)

### Misc Other Notes
* https://vivonomicon.com/2018/06/17/drawing-to-a-small-tft-display-the-ili9341-and-stm32/

## Implementation

//SPI INSTANCE
#define HSPI_INSTANCE							&hspi5

//CHIP SELECT PIN AND PORT, STANDARD GPIO
#define LCD_CS_PORT								GPIOC
#define LCD_CS_PIN								CS_Pin

//DATA COMMAND PIN AND PORT, STANDARD GPIO
#define LCD_DC_PORT								GPIOC
#define LCD_DC_PIN								DC_Pin

//RESET PIN AND PORT, STANDARD GPIO
#define	LCD_RST_PORT							GPIOC
#define	LCD_RST_PIN								RST_Pin

#define TP_CLK_PORT								GPIOE
#define TP_CLK_PIN								T_CLK_Pin

#define TP_CS_PORT								GPIOE
#define TP_CS_PIN								T_CS_Pin

#define TP_MISO_PORT							GPIOE
#define TP_MISO_PIN								T_MISO_Pin

#define TP_MOSI_PORT							GPIOE
#define TP_MOSI_PIN								T_MOSI_Pin

#define TP_IRQ_PORT								GPIOE
#define TP_IRQ_PIN								T_IRQ_Pin